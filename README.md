# React Tools

Simple toolset to help with writing React.js code

## Installation

```
git clone https://gitlab.com/dbustorff/react-tools.git
cd react-tools
npm install && npm build
npm install -g .
```

## Usage

```
component Component1Name Component2Name ... --dir --css --states
```

Use the component function to create new React.js components with the inserted names

### Parameters

- --dir creates a directory for the newly created files
- --css creates and imports a .module.css file
- --states adds the required import for state management
- --js creates .js files (default: .jsx)

## Uninstall

```
npm uninstall -g react-tools
```
