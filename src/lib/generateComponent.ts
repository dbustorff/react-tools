import { existsSync, promises as fsPromises } from 'fs';
import { FileHandle } from 'fs/promises';
import { EOL } from 'os';
import yargs from 'yargs';

const statesImport = `import { useState } from 'react'; ${EOL + EOL}`;
const cssImport = `import styles from "./__CLASS_NAME__.module.css";${
  EOL + EOL
}`;
let componentText = `const __CLASS_NAME__: React.FC = (props) => {${EOL}\treturn(<></>);${EOL}};${
  EOL + EOL
}export default __CLASS_NAME__;`;

const generateComponent = async () => {
  const argv = await yargs(process.argv).parse();
  const classNames: (string | number)[] = argv._.slice(2);

  if (classNames.length === 0) {
    return console.log('No filename, please add at least one');
  }

  if (argv.css) {
    componentText = cssImport + componentText;
  }
  if (argv.states) {
    componentText = statesImport + componentText;
  }

  classNames.forEach(async (name) => {
    let filePath: string = name.toString();

    if (argv.dir) {
      if (!existsSync(name.toString())) {
        try {
          await fsPromises.mkdir(name.toString());
        } catch (error) {
          console.error(error);
        }
      }
      filePath = `${name}/${name}`;
    }
    try {
      if (argv.css) {
        const stylesFile = await fsPromises.open(`${filePath}.module.css`, 'w');
        await stylesFile.close();
      }

      let classFile: FileHandle;

      if (argv.js) {
        classFile = await fsPromises.open(`${filePath}.js`, 'w');
      } else {
        classFile = await fsPromises.open(`${filePath}.tsx`, 'w');
      }
      await classFile.writeFile(
        argv.js
          ? componentText
              .replaceAll('__CLASS_NAME__', name.toString())
              .replaceAll(': React.FC', '')
          : componentText.replaceAll('__CLASS_NAME__', name.toString())
      );
      await classFile.close();
    } catch (error) {
      console.error(error);
    }
  });
  console.log('Files created successfuly');
};

export default generateComponent;
